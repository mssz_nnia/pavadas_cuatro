
import sqlite3

from PyQt5.QtWidgets import QDialog

from dialogo import Ui_Dialog

class dialogVentana(QDialog):

    def __init__(self):

        super(dialogVentana, self).__init__()

        self.ui = Ui_Dialog()

        self.ui.setupUi(self)

        self.mostrarDialogo()

        self.ui.btnConfirmar.clicked.connect(self.clickBtnConfirma)

    
    def clickBtnConfirma(self, conn):

        insertarArticulo = [
            None,
            self.ui.leNroArt.text(),
            self.ui.leDesc.text(),
            self.ui.lePrecio.text()
            ]

        conn = sqlite3.connect('baseDB.db')

        curs = conn.cursor()

        curs.execute('INSERT INTO articulo VALUES (?,?,?,?)', insertarArticulo)

        conn.commit()

        conn.close()


    def mostrarDialogo(self):

        self.show()